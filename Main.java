package com.qatestlabgame;

import com.qatestlabgame.characters.*;
import com.qatestlabgame.characters.Character;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Game process
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<com.qatestlabgame.characters.Character> team1 = new ArrayList<Character>();
        ArrayList<Character> team2 = new ArrayList<Character>();
        create(team1, "human_elf");
        create(team2, "dead_orc");

        /* Случайный выбор команды для 1 хода и запуск пошаговой игры */
        boolean choice = new Random().nextBoolean();
        while (true) {
            if (choice) {
                team1 = prepare(team1);
                try {
                    turn(team1, team2);
                    team2 = prepare(team2);
                }
                catch (Exception e) {
                    break;
                }
                choice = false;
            } else {
                team2 = prepare(team2);
                try {
                    turn(team2, team1);
                    team1 = prepare(team1);
                }
                catch (Exception e) {
                    break;
                }
                choice = true;
            }
        }
        toLog(log);
    }

    private static void turn(ArrayList<Character> team1, ArrayList<Character> team2) {
        for (Character item : team1) {
            Character victim = getVictim(team2);
            if (victim != null) {
                switch (item.getClass().getSimpleName()) {
                    case "DeadWizard":
                        if (new Random().nextBoolean()) {
                            item.shoot(getVictim(team2)); // наслать недуг (уменьшение силы урона персонажа противника на 50% на один ход)
                        } else {
                            item.attack(getVictim(team2)); //атака (нанесение урона 5 HP)
                        }
                        break;

                    case "ElfWizard":
                    case "OrcWizard":
                    case "HumanWizard":
                        if (new Random().nextBoolean()) {
                            item.shoot(getVictim(team1)); // улучшение
                        } else {
                            item.attack(getVictim(team2));
                        }
                        break;

                    default:
                        item.action(getVictim(team2));
                        break;
                }
                log += item.getLog();
            } else {
                throw new NullPointerException("Empty victim!");
            }
        }
    }

    /* Записываем ход игры в файл */
    private static void toLog(String log) {
        try (FileOutputStream fos = new FileOutputStream("qatestlabgame.log")) {
            byte[] buffer = log.getBytes();
            fos.write(buffer);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Перемешиваем команду и сортируем: сначала привелегированные, потом остальные. Убираем "умерших" */
    private static ArrayList<Character> prepare(ArrayList<Character> team) {
        Collections.shuffle(team);
        ArrayList<Character> result = new ArrayList<Character>();
        for (Character item : team) {
            if (item.getLife() > 0 && item.getIsPrivilege()) {
                result.add(0, item);
            } else if (item.getLife() > 0){
                result.add(item);
            }
        }
        return result;
    }

    public static String log="";

    /* Выбор случайного персонажа из команды */
    private static Character getVictim(ArrayList<Character> team) {
        team = prepare(team);
        if (team.size() > 0) {
            Character result = team.get(new Random().nextInt(team.size()));
            return (result.getLife() > 0 ? result : null);
        } else {
            return null;
        }
    }

    /* Создаем две команды случайным образом из Людей или Эльфов и Орков или Нежити */
    private static void create(ArrayList<Character> team, String teamType) {
        Random random = new Random();
        switch (teamType) {
            case "human_elf" :
                if (random.nextBoolean()) {
                    team.add(new HumanWizard());
                    for (int i = 0; i < 3; i++) {
                        team.add(new HumanShooter());
                    }
                    for (int i = 0; i < 4; i++) {
                        team.add(new HumanWarrior());
                    }
                } else {
                    team.add(new ElfWizard());
                    for (int i = 0; i < 3; i++) {
                        team.add(new ElfShooter());
                    }
                    for (int i = 0; i < 4; i++) {
                        team.add(new ElfWarrior());
                    }
                }
                break;
            case "dead_orc" :
                if (random.nextBoolean()) {
                    team.add(new DeadWizard());
                    for (int i = 0; i < 3; i++) {
                        team.add(new DeadShooter());
                    }
                    for (int i = 0; i < 4; i++) {
                        team.add(new DeadWarrior());
                    }
                } else {
                    team.add(new OrcWizard());
                    for (int i = 0; i < 3; i++) {
                        team.add(new OrcShooter());
                    }
                    for (int i = 0; i < 4; i++) {
                        team.add(new OrcWarrior());
                    }
                }
                break;
            default:
        }
    }
}
