package com.qatestlabgame.characters;

import java.util.Random;

/**
 * Стрелок
 */
public interface Shooter {
    void shoot(Character character);
    void attack(Character character);
}