package com.qatestlabgame.characters;

import java.lang.*;
import java.util.Random;

/**
 * Орк маг
 */
public class OrcWizard extends Character implements Wizard {
    String name;
    String shootCaption;
    String attackCaption;
    double life;
    boolean isPrivilege;
    boolean isDamned;

    public OrcWizard() {
        this.name = "Орк шаман";
        this.shootCaption = "улучшение";
        this.attackCaption = "проклятие";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
        this.isDamned = super.isDamned;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public void shoot(Character character) {
        if (!character.getIsPrivilege()) {
            character.setIsPrivilege(true); // наложение улучшения на персонажа своего отряда.
        }
        toConsole(shootCaption, 0, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public void attack(Character character) {
        if (character.getIsPrivilege()) {
            character.setIsPrivilege(false); // наложение проклятия (снятие улучшения с персонажа противника для следующего хода)
        }
        toConsole(attackCaption, 0, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public void action(Character character) {
    }

    @Override
    public boolean isWizard() {
        return true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }
}
