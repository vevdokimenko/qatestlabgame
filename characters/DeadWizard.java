package com.qatestlabgame.characters;

import java.lang.*;
import java.util.Random;

/**
 * Нежить маг
 */
public class DeadWizard extends Character implements Wizard {
    String name;
    String shootCaption;
    String attackCaption;
    double life;
    boolean isPrivilege;
    boolean isDamned;
    double damage;

    public DeadWizard() {
        this.name = "Нежить некромант";
        this.shootCaption = "недуг";
        this.attackCaption = "магия";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
        this.isDamned = super.isDamned;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public void shoot(Character character) {
        if (!character.getIsDamned())
            character.setIsDamned(true); // наслать недуг (уменьшение силы урона персонажа противника на 50% на один ход)
        toConsole(shootCaption, damage, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public void attack(Character character) {
        damage = 5; // атака (нанесение урона 5 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }

    @Override
    public void action(Character character) {
    }

    @Override
    public boolean isWizard() {
        return true;
    }
}
