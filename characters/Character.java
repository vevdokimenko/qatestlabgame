package com.qatestlabgame.characters;

/**
 * Базовый абстрактный класс персонажа
 */
public abstract class Character {
    int life = 100; // уровень жизни равный 100 HP
    String log = "";
    boolean isPrivilege = false;
    boolean isDamned = false;
    public abstract double getLife();
    public abstract void setLife(double value);
    public abstract String getName();
    public abstract String getShootCaption();
    public abstract String getAttackCaption();
    public abstract boolean getIsPrivilege();
    public abstract void setIsPrivilege(boolean isPrivilege);
    public abstract boolean getIsDamned();
    public abstract void setIsDamned(boolean isDamned);
    public abstract void action(Character character);
    public abstract void attack(Character character);
    public abstract void shoot(Character character);
    public abstract boolean isWizard();

    public void toConsole(String caption, double damage, Character character){
        String message =
            getName() +
            "[" + getLife() + "]\t"+
            caption +
            (damage == 0 ? "" : "(" + damage + ")") +
            "\t--->\t" +
            character.getName() +
            "[" + character.getLife() + "]" +
            (character.getLife() <= 0 ? "\t[умер]" : "") +
            "\r\n";
            log = message;
        System.out.print(message);
    }

    public String getLog(){
        return log;
    }
}