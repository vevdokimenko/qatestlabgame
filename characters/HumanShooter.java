package com.qatestlabgame.characters;


import java.lang.*;
import java.util.Random;

/**
 * Человек Арбалетчик
 */
public class HumanShooter extends Character implements Shooter {
    private String name;
    private String shootCaption;
    private String attackCaption;
    private double life;
    private boolean isPrivilege;
    private double damage;
    private boolean isDamned;

    public HumanShooter() {
        this.name = "Человек арбалетчик";
        this.shootCaption = "арбалет";
        this.attackCaption = "атака";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
        this.isDamned = super.isDamned;
    }

    @Override
    public void shoot(Character character) {
        damage = 5;
        character.setLife(character.getLife() - damage); // стрелять из арбалета (нанесение урона 5 HP)
        toConsole(shootCaption, damage, character);
    }

    @Override
    public void attack(Character character) {
        damage = 3; // атаковать (нанесение урона 3 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
    }

    @Override
    public void action(Character character) {
        Random random = new Random();
        if (random.nextBoolean()){
            shoot(character);
        } else
            attack(character);
        isPrivilege = false;
        isDamned = false;
    }

    @Override
    public boolean isWizard() {
        return false;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }
}
