package com.qatestlabgame.characters;


import java.lang.*;
import java.util.Random;

/**
 * Орк лучник
 */
public class OrcShooter extends Character implements Shooter {
    String name;
    String shootCaption;
    String attackCaption;
    double life;
    boolean isPrivilege;
    double damage;
    boolean isDamned;

    public OrcShooter() {
        this.name = "Орк лучник";
        this.shootCaption = "лук";
        this.attackCaption = "клинок";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
        this.isDamned = super.isDamned;
    }

    @Override
    public void shoot(Character character) {
        damage = this.getIsPrivilege() ? 3 * 1.5 : 3;
        character.setLife(character.getLife() - damage); // стрелять из лука (нанесение урона 3 HP)
        toConsole(shootCaption, damage, character);
    }

    @Override
    public void attack(Character character) {
        damage = 2; // удар клинком (нанесение урона 2 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
    }

    @Override
    public void action(Character character) {
        Random random = new Random();
        if (random.nextBoolean()){
            shoot(character);
        } else
            attack(character);
        isPrivilege = false;
        isDamned = false;
    }

    @Override
    public boolean isWizard() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }
}
