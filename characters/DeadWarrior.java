package com.qatestlabgame.characters;

import java.lang.*;

/**
 * Нежить воин
 */
public class DeadWarrior extends Character implements Warrior {
    private String name;
    private String attackCaption;
    private double life;
    boolean isPrivilege;
    private boolean isDamned;
    private double damage;

    public DeadWarrior() {
        this.name = "Нежить зомби";
        this.attackCaption = "копье";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return null;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public void attack(Character character) {
        damage = 18; // удар копьем (нанесение урона 18 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
    }

    @Override
    public void shoot(Character character) {
        attack(character);
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }

    @Override
    public void action(Character character) {
        attack(character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public boolean isWizard() {
        return false;
    }
}
