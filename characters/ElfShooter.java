package com.qatestlabgame.characters;


import java.lang.*;
import java.util.Random;

/**
 * Эльф лучник
 */
public class ElfShooter extends Character implements Shooter {
    private String name;
    private String shootCaption;
    private String attackCaption;
    private double life;
    private boolean isPrivilege;
    private boolean isDamned;
    private double damage;

    public ElfShooter() {
        this.name = "Эльф лучник";
        this.shootCaption = "лук";
        this.attackCaption = "атака";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public void shoot(Character character) {
        damage = this.getIsPrivilege() ? 7 * 1.5 : 7;
        character.setLife(character.getLife() - damage); // стрелять из лука (нанесение урона 7 HP)
        toConsole(shootCaption, damage, character);
    }

    @Override
    public void attack(Character character) {
        damage = 2; // атаковать противника (нанесение урона 3 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
    }

    @Override
    public void action(Character character) {
        Random random = new Random();
        if (random.nextBoolean()){
            shoot(character);
        } else
            attack(character);
        isPrivilege = false;
        isDamned = false;
    }

    @Override
    public boolean isWizard() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }
}
