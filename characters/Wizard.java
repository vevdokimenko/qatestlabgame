package com.qatestlabgame.characters;

/**
 * Маг
 */
public interface Wizard {
    void shoot(Character character);
    void attack(Character character);
}
