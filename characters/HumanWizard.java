package com.qatestlabgame.characters;

import java.lang.*;
import java.util.Random;

/**
 * Человек маг
 */
public class HumanWizard extends Character implements Wizard {
    private String name;
    private String shootCaption;
    private String attackCaption;
    private double life;
    private boolean isPrivilege;
    private double damage;
    private boolean isDamned;

    public HumanWizard() {
        this.name = "Человек маг";
        this.shootCaption = "улучшение";
        this.attackCaption = "магия";
        this.life = super.life;
        this.isPrivilege = super.isPrivilege;
        this.isDamned = super.isDamned;
    }

    @Override
    public double getLife() {
        return life;
    }

    @Override
    public void setLife(double value) {
        life = value;
    }

    @Override
    public void shoot(Character character) {
        if (!character.getIsPrivilege()) {
            character.setIsPrivilege(true); // наложение улучшения на персонажа своего отряда
        }
        toConsole(shootCaption, damage, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public void attack(Character character) {
        damage = 4; // атаковать магией (нанесение урона 4 HP)
        damage = this.getIsPrivilege() ? damage * 1.5 : damage;
        damage = this.getIsDamned() ? damage / 2 : damage;
        character.setLife(character.getLife() - damage);
        toConsole(attackCaption, damage, character);
        isDamned = false;
        isPrivilege = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShootCaption() {
        return shootCaption;
    }

    @Override
    public String getAttackCaption() {
        return attackCaption;
    }

    @Override
    public boolean getIsPrivilege() {
        return isPrivilege;
    }

    @Override
    public void setIsPrivilege(boolean isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    @Override
    public boolean getIsDamned() {
        return isDamned;
    }

    @Override
    public void setIsDamned(boolean isDamned) {
        this.isDamned = isDamned;
    }

    @Override
    public void action(Character character) {
    }

    @Override
    public boolean isWizard() {
        return true;
    }
}
