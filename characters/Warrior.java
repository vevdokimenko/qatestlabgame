package com.qatestlabgame.characters;

/**
 * Воин
 */
public interface Warrior {
    void attack(Character character);
}
